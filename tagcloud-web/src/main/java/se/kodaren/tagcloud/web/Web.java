package se.kodaren.tagcloud.web;

import static spark.Spark.get;
import static spark.Spark.staticFileLocation;

/**
 * Application that serves web resources.
 */
public class Web {
    public static void main(String[] args) {
        // Specify static resources
        staticFileLocation("/public");

        get("/", (req, res) -> null);
    }
}
