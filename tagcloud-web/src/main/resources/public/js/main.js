$(document).ready(function() {
    var host = "http://kodaren.se:8000";
    var tagsResource = host + "/tags";
    var searchResource = host + "/search";

    $("#search-input").keydown(function(event){
        if(event.keyCode==13){
           $("#search-btn").trigger("click");
        }
    });

    $("#search-btn").click(function() {
        var searchTerm = $("#search-input").val();
        $.getJSON(searchResource,
        {
            query: searchTerm,
        },
         function(data) {
            var items = [];
            $.each(data, function(key, val) {
                items.push("<div><h3>" + val.title + "</h3><p>" + val.extract + "</p></div>");
            });

            $("#articles").empty();

            $("<div/>", {
                "class": "articles",
                html: items.join("")
            }).appendTo("#articles");

            $.getJSON(tagsResource,
                function(data) {
                    updateTagCloud(data);
                });
         });
    });

    $.getJSON(tagsResource,
        function(data) {
            updateTagCloud(data);
        }
    );
});

/**
 * Updates the content of the tagcloud with the specified tags.
 */
function updateTagCloud(tags) {
    var items = [];
    var total = 0;
    $.each(tags, function(key, tag) {
        total += tag.count;
        items.push(tag);
    });

    $("#tagcloud").empty();

    $.each(items, function(index, tag) {
        $('<span/>', {
            "class": "tag",
            'text': tag.value,
            'style': "font-size: " + calculatePercentage(tag, total) * 2 + "%"
        }).appendTo('#tagcloud');
    });
}

/**
 * Calculates the percentage of how often a tag has been searched for.
 */
function calculatePercentage(tag, totalCount) {
    return (1 + tag.count / totalCount) * 100;
}