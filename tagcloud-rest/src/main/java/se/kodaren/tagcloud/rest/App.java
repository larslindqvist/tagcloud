package se.kodaren.tagcloud.rest;

import com.google.gson.Gson;
import se.kodaren.tagcloud.rest.tag.Tag;
import se.kodaren.tagcloud.rest.tag.TagDAO;
import se.kodaren.tagcloud.rest.wikipedia.Page;
import se.kodaren.tagcloud.rest.wikipedia.WikipediaDAO;

import java.util.List;

import static spark.Spark.*;

/**
 *
 */
public class App {
    public static void main(String[] args) {
        TagDAO tagDAO = new TagDAO();

        // Define route for the "/search" resource
        get("/search", (req, res) -> {
            String query = req.queryMap("query").value();

            Tag tag = new Tag(query.toUpperCase());
            if(!tagDAO.upsertAndIncrement(tag)) {
                tagDAO.insert(tag);
            }
            List<Page> pages = WikipediaDAO.search(query);

            System.out.println("Tag: " + ": " + query);
            return new Gson().toJson(pages);
        });

        // Define route for the "/tags" resource
        get("/tags", (req, res) -> {
            List<Tag> tags = tagDAO.findAll();
            System.out.println("Tags: " + tags.size());
            return new Gson().toJson(tags);
        });

        // A filter that is run after before sending the response
        after((req, res) -> {
            res.type("application/json");
            res.header("Access-Control-Allow-Origin", "*");
        });
    }
}
