package se.kodaren.tagcloud.rest.tag;

/**
 *
 */
public class Tag {
    private String value;
    private int count;

    public Tag(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
