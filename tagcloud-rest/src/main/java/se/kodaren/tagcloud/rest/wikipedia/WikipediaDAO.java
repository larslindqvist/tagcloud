package se.kodaren.tagcloud.rest.wikipedia;

import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A DAO class that can be used to get article information from Wikipedia's API.
 */
public class WikipediaDAO {
    private static Logger logger = LoggerFactory.getLogger(WikipediaDAO.class);
    private static String API_URL = "https://sv.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles={0}";
    private static String MISSING_FLAG = "missing";

    public static List<Page> search(String searchTerm) {
        List<Page> pages = new ArrayList<>();
        try {
            URL url = new URL(API_URL.replace("{0}", searchTerm));
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject jsonObject = root.getAsJsonObject().get("query").getAsJsonObject();

            for(Map.Entry<String, JsonElement> entry : jsonObject.get("pages").getAsJsonObject().entrySet()) {
                // Only deserialize the entry if it exists (missing == null)
                if(entry.getValue().getAsJsonObject().get(MISSING_FLAG) == null) {
                    pages.add(new Gson().fromJson(entry.getValue(), Page.class));
                }
            }
        }
        catch (IOException e) {
            logger.error(e.getMessage());
        }

        return pages;
    }
}
