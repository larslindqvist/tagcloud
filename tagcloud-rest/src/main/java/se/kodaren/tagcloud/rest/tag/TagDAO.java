package se.kodaren.tagcloud.rest.tag;

import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;

import java.util.*;

/**
 * A DAO that can be used to fetch and modify {@link Tag}s in a MongoDB database.
 */
public class TagDAO {
    public static final String DB_NAME = "tagcloud";
    public static final String TABLE_TAGS = "tags";

    public static final String FIELD_VALUE = "value";
    public static final String FIELD_COUNT = "count";

    public static final String CONFIG_DATABASE_URI = "database.uri";

    private MongoClient mongo;
    private MongoDatabase database;
    private MongoCollection<Document> tagsTable;

    public TagDAO() {
        String databaseUri = System.getProperty(CONFIG_DATABASE_URI);
        if(databaseUri == null) {
            databaseUri = "mongodb://db:27017";
        }
        mongo = new MongoClient(new MongoClientURI(databaseUri));
        database = mongo.getDatabase(DB_NAME);
        tagsTable = database.getCollection(TABLE_TAGS);
    }

    /**
     * Inserts a new {@link Tag} into the database.
     *
     * @param tag The tag to insert
     */
    public void insert(Tag tag) {
        tagsTable.insertOne(new Document().append(FIELD_VALUE, tag.getValue()).append(FIELD_COUNT, 1));
    }

    /**
     * Upserts a Tag. If it exists, its {@link #FIELD_COUNT} field will be incremented.
     *
     * @param tag The tag to be upserted and incremented
     * @return <code>true</code> if the tag was modified
     */
    public boolean upsertAndIncrement(Tag tag) {
        UpdateResult updateResult = tagsTable.updateOne(new Document().append(FIELD_VALUE, tag.getValue()), new Document("$inc", new Document(FIELD_COUNT, 1)));
        System.out.println("UpdateResult: " + updateResult.toString());
        return updateResult.getModifiedCount() != 0;
    }

    /**
     * Find all {@link Tag}s.
     *
     * @return A list of tags
     */
    public List<Tag> findAll() {
        List<Tag> all = new ArrayList<>();

        try (MongoCursor<Document> cursor = tagsTable.find().iterator()) {
            while (cursor.hasNext()) {
                Document tag = cursor.next();
                all.add(new Gson().fromJson(tag.toJson(), Tag.class));
            }
        }

        return all;
    }
}
