package se.kodaren.tagcloud.rest;

import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.Tag;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.*;
import se.kodaren.tagcloud.rest.tag.*;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static se.kodaren.tagcloud.rest.tag.TagDAO.CONFIG_DATABASE_URI;

/**
 * Integration tests for {@link TagDAO}.
 *
 * These tests require a MongoDB instance running at {@value #DATABASE_URI}.
 *
 */
public class TagDAOITCase {
    private static final String DATABASE_URI = "mongodb://localhost:27017";
    private static final String TAG_VALUE = "Java";
    private MongoCollection<Document> tags;

    @BeforeClass
    public static void setUpOnce() throws Exception {
        System.setProperty(CONFIG_DATABASE_URI, DATABASE_URI);
    }

    @Before
    public void setUp() throws Exception {
        MongoClient mongo = new MongoClient(new MongoClientURI(System.getProperty(CONFIG_DATABASE_URI)));
        MongoDatabase database = mongo.getDatabase(TagDAO.DB_NAME);
        tags = database.getCollection(TagDAO.TABLE_TAGS);
        tags.drop();
    }

    /**
     * Tests inserting a {@link se.kodaren.tagcloud.rest.tag.Tag}.
     *
     * Before insert: Empty table
     * After insert: One document in table:
     * {
     *  "value": "Java",
     *  "count": 1
     *  }
     *
     * @throws Exception
     */
    @Test
    public void testInsert() throws Exception {
        se.kodaren.tagcloud.rest.tag.Tag tag = new se.kodaren.tagcloud.rest.tag.Tag(TAG_VALUE);
        TagDAO tagDAO = new TagDAO();
        tagDAO.insert(tag);

        assertEquals(tags.count(), 1);
        Document javaTag = tags.find().first();
        se.kodaren.tagcloud.rest.tag.Tag result = new Gson().fromJson(javaTag.toJson(), se.kodaren.tagcloud.rest.tag.Tag.class);

        assertEquals(tag.getValue(), result.getValue());
        assertTrue(result.getCount() == 1);
    }

    /**
     * Tests upserting a {@link se.kodaren.tagcloud.rest.tag.Tag} and incrementing its {@link se.kodaren.tagcloud.rest.tag.Tag#count} value.
     *
     * @throws Exception
     */
    @Test
    public void testUpsertAndIncrement() throws Exception {
        se.kodaren.tagcloud.rest.tag.Tag tag = new se.kodaren.tagcloud.rest.tag.Tag(TAG_VALUE);
        TagDAO tagDAO = new TagDAO();

        assertFalse(tagDAO.upsertAndIncrement(tag));
        tagDAO.insert(tag);
        assertTrue(tagDAO.upsertAndIncrement(tag));
    }

    /**
     * Tests {@link TagDAO#findAll()} by asserting that the tags collection is empty until a Tag is inserted.
     *
     * @throws Exception
     */
    @Test
    public void testFindAll() throws Exception {
        se.kodaren.tagcloud.rest.tag.Tag tag = new se.kodaren.tagcloud.rest.tag.Tag(TAG_VALUE);
        TagDAO tagDAO = new TagDAO();

        List<se.kodaren.tagcloud.rest.tag.Tag> allTags = tagDAO.findAll();
        assertTrue(allTags.size() == 0);

        tagDAO.insert(tag);

        allTags = tagDAO.findAll();
        assertTrue(allTags.size() == 1);
        assertEquals(TAG_VALUE, allTags.get(0).getValue());
        assertTrue(allTags.get(0).getCount() == 1);
    }
}
