package se.kodaren.tagcloud.rest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import se.kodaren.tagcloud.rest.tag.Tag;
import spark.Spark;

import java.io.InputStreamReader;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static se.kodaren.tagcloud.rest.tag.TagDAO.CONFIG_DATABASE_URI;

/**
 * Integration tests for the REST API.
 *
 * These tests require a MongoDB instance running at {@value #DATABASE_URI}.
 */
public class AppITCase {
    private static final String DATABASE_URI = "mongodb://localhost:27017";

    /**
     * Set up and start the rest service.
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpOnce() throws Exception {
        System.setProperty(CONFIG_DATABASE_URI, DATABASE_URI);
        App.main(null);
        RestAssured.port = 4567;
        RestAssured.baseURI = "http://localhost";
    }

    /**
     * Tear down the rest service.
     */
    @AfterClass
    public static void afterClass() {
        Spark.stop();
    }

    /**
     * Validates the response when searching for an existing page.
     * @throws Exception
     */
    @Test
    public void testSearchExistingPage() throws Exception {
        String pageTitle = "Java";
        Response response = given().get("/search?query=" + pageTitle);
        assertEquals(response.statusCode(), 200);
        assertEquals(response.getContentType(), ContentType.JSON.toString());
        assertNotNull(response.path("title"));
        assertNotNull(response.path("pageid"));
        assertNotNull(response.path("extract"));
    }

    /**
     * Validates the response when searching for a non-existing page.
     * @throws Exception
     */
    @Test
    public void testSearchNonexistingPage() throws Exception {
        String pageTitle = "JavaJava";
        Response response = given().get("/search?query=" + pageTitle);
        assertEquals(response.statusCode(), 404);
        assertEquals(response.getContentType(), ContentType.JSON.toString());
    }

    /**
     * Validates the response when searching for a non-existing page.
     * @throws Exception
     */
    @Test
    public void testTags() throws Exception {
        Response response = given().get("/tags");
        assertEquals(response.statusCode(), 200);
        assertEquals(response.getContentType(), ContentType.JSON.toString());
        List<Tag> tags = new Gson().fromJson(new InputStreamReader(response.getBody().asInputStream()), new TypeToken<List<Tag>>(){}.getType());
        assertNotNull(tags);
    }
}
