# Tagcloud

## Description

Tagcloud is a web project by Lars Lindqvist. It displays a tagcloud based on searches made by the users.

The project consists of two separate Maven modules:
* tagcloud-rest: A REST API
* tagcloud-web: Web resources

## Software used
* Database: MongoDB
* REST-service: SparkJava
* Web: Bootstrap, JQuery

## Build requirements
* Maven
* Docker
* docker-compose

## Build

```
mvn clean package
```

## Start all components

```
docker-compose up
```

## Default configuration (docker-compose)

MongoDB is created from the [mongo Docker image](https://hub.docker.com/_/mongo/).

REST API can be reached at http://rest:8000 from within containers, however since tagcloud-web calls the API
through JavaScript http://127.0.0.1:8000 is used. Currently, this sets a requirement that tagcloud-web and tagcloud-rest
is on the same host.

### Uri and Ports
* MongoDB
    * Within containers: mongodb://db:27017
    * External: mongodb://<host>:27017
* tagcloud-rest:
    * Within containers: http://rest:4567
    * External: http://<host>:8000
* tagcloud-web:
    * Within containers: http://web:4567
    * External: http://<host>:8080
